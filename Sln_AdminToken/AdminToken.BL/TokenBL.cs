﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdminToken.DA;
using AdminToken.BE.Parameters;
using AdminToken.BE.Result;
namespace AdminToken.BL
{
    public class TokenBL
    {
        TokenDA objTokenDA;
        public ResultValidarTokenBE ValidarToken(TokenBE objTokenBE)
        {
            objTokenDA = new TokenDA();
            return objTokenDA.ValidarToken(objTokenBE);
        }

        public ResultGenerarTokenBE GenerarToken(DateTime fechaGeneracion, DateTime fechaExpiracion)
        { 
            ResultGenerarTokenBE objResultGenerarTokenBE  = new ResultGenerarTokenBE();
            ResultRegistrarTokenBE objResultRegistrarTokenBE = new ResultRegistrarTokenBE();

            ResultGenericoBE objResultGenericoBE = new ResultGenericoBE();

            TokenBE objTokenBE = new TokenBE();
            try
            {
                objTokenBE.TokenSesion = GenerarIdentificadorUnico();
                objTokenBE.FechaGeneracion = fechaGeneracion;
                objTokenBE.FechaExpiracion = fechaExpiracion;

                objResultRegistrarTokenBE = RegistrarToken(objTokenBE);

                if (objResultRegistrarTokenBE.ResultadoEjecucion.Codigo == "0")
                {
                    objResultGenericoBE.Codigo = "0";
                    objResultGenericoBE.Mensaje = "Ejecucion exitosa";                    
                }
                else
                {
                    objResultGenericoBE.Codigo = objResultRegistrarTokenBE.ResultadoEjecucion.Codigo;
                    objResultGenericoBE.Mensaje = objResultRegistrarTokenBE.ResultadoEjecucion.Mensaje; 
                }
                objResultGenerarTokenBE.TokenGenerado = objTokenBE.TokenSesion;
                objResultGenerarTokenBE.ResultadoEjecucion = objResultGenericoBE;
            }
            catch (Exception ex)
            {
                objResultGenericoBE.Codigo = "-3";
                objResultGenericoBE.Mensaje = ex.Message;
                objResultGenerarTokenBE.ResultadoEjecucion = objResultGenericoBE;
            }
            return objResultGenerarTokenBE;
        }

        private ResultRegistrarTokenBE RegistrarToken(TokenBE objTokenBE)
        {
            objTokenDA = new TokenDA();
            return objTokenDA.RegistrarToken(objTokenBE);        
        }

        private string GenerarIdentificadorUnico()
        {
            DateTime datTime = DateTime.Now;
            string strIdentAdc;

            strIdentAdc = datTime.ToShortDateString() + datTime.ToLongTimeString();
            strIdentAdc = strIdentAdc.Replace("/", "").Replace(":", "").Replace(" ", "").Replace(".", "");
            return Guid.NewGuid().ToString() + "-" + strIdentAdc.ToUpper();
        }
    }
}
