﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminToken.BE.Result
{
    public class ResultGenericoBE
    {
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
    }
}
