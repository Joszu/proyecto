﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminToken.BE.Result
{
    public class ResultGenerarTokenBE
    {
        public ResultGenericoBE ResultadoEjecucion { get; set; }
        public string TokenGenerado { get; set; }
    }
}
