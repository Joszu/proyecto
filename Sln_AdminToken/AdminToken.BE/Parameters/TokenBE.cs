﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdminToken.BE.Parameters
{
    public class TokenBE
    {
        public string Usuario { get; set; }
        public string TokenSesion { get; set; }
        public DateTime FechaGeneracion { get; set; }
        public DateTime FechaExpiracion { get; set; }
    }
}
