﻿using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
namespace AdminToken.DA
{
    internal class DataBaseHelper
    {
        internal static string Comparar(string cadena1, string cadena2)
        {
            var _keySize = 2048;
            using (var rsaCryptoServiceProvider = new RSACryptoServiceProvider(_keySize))
            {
                rsaCryptoServiceProvider.FromXmlString(cadena2);
                int base64BlockSize = ((_keySize / 8) % 3 != 0) ? (((_keySize / 8) / 3) * 4) + 4 : ((_keySize / 8) / 3) * 4;
                int iterations = cadena1.Length / base64BlockSize;
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < iterations; i++)
                {
                    byte[] encryptedBytes = Convert.FromBase64String(
                        cadena1.Substring(base64BlockSize * i, base64BlockSize));
                    Array.Reverse(encryptedBytes);
                    arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(
                        encryptedBytes, true));
                }
                return Encoding.UTF32.GetString(arrayList.ToArray(Type.GetType("System.Byte")) as byte[]);
            }
        }
    }
}
