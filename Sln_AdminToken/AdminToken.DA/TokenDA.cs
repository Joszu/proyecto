﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using AdminToken.BE.Parameters;
using AdminToken.BE.Result;

namespace AdminToken.DA
{
    public class TokenDA
    {
        private ConnectionDA CadenasConexion;
        private string _strCnnOracle = string.Empty;

        private void SurroundingClass()
        {
            CadenasConexion = new ConnectionDA();
            _strCnnOracle = CadenasConexion.StrConnOracleINSUDB;
            CadenasConexion = null;
        }

        public ResultValidarTokenBE ValidarToken(TokenBE objTokenBE)
        {
            ResultValidarTokenBE objResultValidarTokenBE = new ResultValidarTokenBE();
            ResultGenericoBE objResultGenericoBE = new ResultGenericoBE();
            

            try
            {
                SurroundingClass();
                using (OracleConnection conexion = new OracleConnection(_strCnnOracle))
                {
                    OracleCommand cmd = new OracleCommand();

                    cmd.CommandText = "INSUDB.USP_VALIDAR_IDENTIFICADOR";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("V_USUARIO", OracleDbType.Varchar2, 100).Value = objTokenBE.Usuario;
                    cmd.Parameters.Add("V_TOKEN_SESION_USR", OracleDbType.Varchar2, 100).Value = objTokenBE.TokenSesion;
                    cmd.Parameters.Add("V_COD_RPTA", OracleDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("V_MSG_RPTA", OracleDbType.Varchar2,100).Direction = ParameterDirection.Output;

                    conexion.Open();
                    cmd.ExecuteNonQuery();
                   
                    objResultGenericoBE.Codigo = cmd.Parameters["V_COD_RPTA"].Value.ToString();
                    objResultGenericoBE.Mensaje = cmd.Parameters["V_MSG_RPTA"].Value.ToString();
                    objResultValidarTokenBE.ResultadoEjecucion = objResultGenericoBE;

                    conexion.Close();
                    cmd.Dispose();
                }
            }
            catch (Exception ex)
            {
                objResultGenericoBE.Codigo = "-3";
                objResultGenericoBE.Mensaje = ex.Message;
                objResultValidarTokenBE.ResultadoEjecucion = objResultGenericoBE;

            }
            return objResultValidarTokenBE;
        }

        public ResultRegistrarTokenBE RegistrarToken(TokenBE objTokenBE)
        {
            ResultRegistrarTokenBE objResultRegistrarTokenBE = new ResultRegistrarTokenBE();
            ResultGenericoBE objResultGenericoBE = new ResultGenericoBE();

            try
            {
                SurroundingClass();
                using (OracleConnection conexion = new OracleConnection(_strCnnOracle))
                {
                    OracleCommand cmd = new OracleCommand();

                    cmd.CommandText = "INSUDB.USP_REGISTRAR_IDENTIFICADOR";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("V_TOKEN_SESION_USR", OracleDbType.Varchar2, 100).Value = objTokenBE.TokenSesion;
                    cmd.Parameters.Add("V_FECHA_GENERACION", OracleDbType.Date).Value = objTokenBE.FechaGeneracion;
                    cmd.Parameters.Add("V_FECHA_EXPIRACION", OracleDbType.Date).Value = objTokenBE.FechaExpiracion;
                    cmd.Parameters.Add("V_COD_RPTA", OracleDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("V_MSG_RPTA", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;

                    conexion.Open();
                    cmd.ExecuteNonQuery();

                    objResultGenericoBE.Codigo = cmd.Parameters["V_COD_RPTA"].Value.ToString();
                    objResultGenericoBE.Mensaje = cmd.Parameters["V_MSG_RPTA"].Value.ToString();
                    objResultRegistrarTokenBE.ResultadoEjecucion = objResultGenericoBE;

                    conexion.Close();
                    cmd.Dispose();
                }
            }
            catch (Exception ex)
            {
                objResultGenericoBE.Codigo = "-3";
                objResultGenericoBE.Mensaje = ex.Message;
                objResultRegistrarTokenBE.ResultadoEjecucion = objResultGenericoBE;

            }
            return objResultRegistrarTokenBE;
        }
    }
}
