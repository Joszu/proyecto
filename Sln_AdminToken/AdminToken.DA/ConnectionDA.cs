﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
namespace AdminToken.DA
{
    public class ConnectionDA
    {
        private string _StrConnOracleINSUDB = ConfigurationManager.ConnectionStrings["Cnx_INSUDB"].ConnectionString;

        public string StrConnOracleINSUDB
        {
            get
            {
                return DataBaseHelper.Comparar(_StrConnOracleINSUDB, AdminToken.DA.Resource1.NoSeasSapo);
            }
            set
            {
                if (_StrConnOracleINSUDB == value)
                    return;
                _StrConnOracleINSUDB = value;
            }
        }
    }
}
