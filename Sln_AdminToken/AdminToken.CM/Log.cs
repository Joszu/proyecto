﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
namespace AdminToken.CM
{
    public class Log
    {
        private string strRutaLog = ConfigurationManager.AppSettings["rutalLog"];
        public void grabarLog(string mensaje)
        {
            StreamWriter sWriter = new StreamWriter(strRutaLog, true);            
            sWriter.WriteLine(DateTime.Now.ToString() + "  --  " + mensaje);
            sWriter.Close();
        }
    }
}
