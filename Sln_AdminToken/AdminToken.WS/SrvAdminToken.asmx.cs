﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using AdminToken.BE.Parameters;
using AdminToken.BE.Result;
using AdminToken.BL;
using AdminToken.CM;
namespace AdminToken.WS
{
    /// <summary>
    /// Descripción breve de SrvAdminToken
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class SrvAdminToken : System.Web.Services.WebService
    {
        TokenBL objTokenBL;
        Log objLog;

        [WebMethod]
        public ResultValidarTokenBE ValidarToken(TokenBE objTokenBE)
        {
            objLog = new Log();
            
            ResultValidarTokenBE objResultValidarTokenBE;
            objTokenBL = new TokenBL();

            objLog.grabarLog("Inicio validar token");
            objLog.grabarLog("Datos entrada: usuario:" + objTokenBE.Usuario + ", token=" + objTokenBE.TokenSesion);
            objResultValidarTokenBE = objTokenBL.ValidarToken(objTokenBE);
            objLog.grabarLog("Resultado validar token: cod=" + objResultValidarTokenBE.ResultadoEjecucion.Codigo + ",msg=" + objResultValidarTokenBE.ResultadoEjecucion.Mensaje);
            objLog.grabarLog("Fin generar token");
            return objResultValidarTokenBE;
        }

        [WebMethod]
        public ResultGenerarTokenBE GenerarToken(DateTime fechaGeneracion, DateTime fechaExpiracion)
        {
            objLog = new Log();

            ResultGenerarTokenBE objResultGenerarTokenBE;
            objTokenBL = new TokenBL();
            objLog.grabarLog("Inicio generar token");
            objLog.grabarLog("Datos entrada generar token: fechaGeneracion:" + fechaGeneracion.ToLongDateString() + " " + fechaGeneracion.ToLongTimeString() + ", fechaExpiracion:" + fechaExpiracion.ToLongDateString() + " " + fechaExpiracion.ToLongTimeString());
            objResultGenerarTokenBE = objTokenBL.GenerarToken(fechaGeneracion, fechaExpiracion);
            objLog.grabarLog("Resultado generar token: cod=" + objResultGenerarTokenBE.ResultadoEjecucion.Codigo + ", msg=" + objResultGenerarTokenBE.ResultadoEjecucion.Mensaje + ",token=" + objResultGenerarTokenBE.TokenGenerado);
            objLog.grabarLog("Fin generar token");
            return objResultGenerarTokenBE;
        }
    }
}
